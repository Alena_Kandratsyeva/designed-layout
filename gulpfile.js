var gulp = require('gulp');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var spritesmith = require("gulp.spritesmith");

gulp.task('default', function(){
    gulp.watch('pre-less/**/*.less', ['less']);
});

gulp.task('less', function(){
    gulp.src('pre-less/pre-less.less')
        .pipe(less())
        .pipe(gulp.dest('css'))
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('img/sprite/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../img/sprite.png',
            cssName: 'sprite.less',
            algorithm: 'alt-diagonal',
            padding: 20
        }));
    spriteData.img.pipe(gulp.dest('img'));
    spriteData.css.pipe(gulp.dest('pre-less/components'));
});











