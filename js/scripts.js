$(document).ready(function(){
    var $window = $(window);
    var $menu = $("nav.menu");
    var $hid_cont = $(".hidden_container");
    var $aside = $("aside");
    var $art_1 = $(".article_about p");
    var $art_2 = $(".article_some p");
    var $aside_nav = $(".link_cloud").parent();

    $( ".menu" ).click(function( event ) {

       $el =  $( event.target );
       var  $btn_name = $el.attr('data-block-name');
     // alert( $btn_name);
       if( $btn_name == 'm_wrapp'){
           if($(".nav-btn").hasClass('active')){
               clearAll();
           }else{
               clearAll();
               $(".nav-btn").addClass("active");
               $(".m_wrapp").removeClass("hidden").addClass("visible");
           }
       }
       if( $btn_name =="article_about") {
          if($(".h_about").hasClass("active")){
              clearAll();
          }else{
              clearAll();
              $(".h_about").addClass("active");
              $(".b_about").addClass("active");
              $art_1.addClass("visible");
              if ( $window.width() >= '433' ){
                  $hid_cont.addClass("visible");
                  $aside_nav.removeClass().addClass("visible");
              }
          }
       }
       if($btn_name=="article_some") {
          if($(".h_idea").hasClass("active")){
              clearAll();
          }else{
              clearAll();
              $(".h_idea").addClass("active");
              $(".b_idea").addClass("active");
              $art_2.addClass("visible");
              if ( $window.width() >= '433' ){
                  $hid_cont.addClass("visible");
                  $aside_nav.removeClass().addClass("visible");
              }
          }
       }
       if($btn_name == "link_cloud") {
              if($(".b_aside").hasClass("active")){
              clearAll();
          }else{
              clearAll();
              $(".b_aside").addClass("active");
              $aside_nav.addClass("visible");
              $aside.removeClass().addClass("visible mob_visible");
          }
       }

    });

    function menuFixed() {
        var $navOffsetTop = $window.scrollTop();
        if ( $navOffsetTop > 100) {
            $menu.addClass("fixed");
        } else {
            $menu.removeClass("fixed");
        }
    }

    function clearAll(){
        $('.btn').each(function(){
            $(this).removeClass("active");
        });
        $('.head_sign').each(function(){
            $(this).removeClass("active");
        });
        $('.article p').each(function(){
            $(this).removeClass("visible");
        });
        $hid_cont.removeClass("visible").className = "hidden";
        $(".m_wrapp").removeClass("visible");
        $aside.removeClass("visible mob_visible");
        $aside_nav.removeClass("visible");
    }

    $window.resize(function(){
        clearAll();
    });
    $window.scroll(function(){
        menuFixed();
    });

});



